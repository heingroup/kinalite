from unittest import TestCase
import pandas as pd
import plotly.express as px
import plotly.graph_objects as po
from kinalite.experiment import Experiment
from kinalite.plots import plot_experiment_results


class TestExperiment(TestCase):
    def test_kinugasa(self):
        data = [
            pd.read_csv('./data/kinugasa/exp0.0_ref.csv'),
            pd.read_csv('./data/kinugasa/exp0.1_xs_alkyne.csv'),
        ]

        experiment = Experiment('A', data, substrate_index=2, product_index=3)
        result = experiment.calculate_best_result()
        plot_experiment_results(experiment)
        actual_order = -0.5
        self.assertAlmostEqual(result.order, actual_order, places=1)

    def test_plotting(self):
        exp_1 = pd.read_csv('./data/kinugasa/exp0.0_ref.csv')
        exp_2 = pd.read_csv('./data/kinugasa/exp0.1_xs_alkyne.csv')

        experiment = Experiment('order_in_a', [exp_1, exp_2], substrate_index=2, product_index=3)
        results = experiment.calculate_results()
        best_result = experiment.calculate_best_result()

        scores_fig = px.scatter(x=[r.order for r in results], y=[r.score for r in results], labels={'y': 'Score', 'x': 'Order'})
        scores_fig.show()

        curves_fig = po.Figure()
        curves_fig.add_trace(po.Scatter(x=experiment.best_result.x[0], y=experiment.best_result.y[0], mode='markers', name='Exp 1'))
        curves_fig.add_trace(po.Scatter(x=experiment.best_result.x[1], y=experiment.best_result.y[1], mode='markers', name='Exp 2'))
        curves_fig.update_layout(title='Order in A', xaxis_title=f'Σ[A]^{best_result.order:.2}', yaxis_title='[P]')
        curves_fig.show()

        print(best_result)

    def test_perfect(self):
        exp_1 = pd.read_csv('./data/perfect_data/exp1_ref.csv')
        exp_2 = pd.read_csv('./data/perfect_data/exp2_xs_a.csv')
        exp_3 = pd.read_csv('./data/perfect_data/exp3_xs_b.csv')
        exp_4 = pd.read_csv('./data/perfect_data/exp4_xs_b_and_cat.csv')
        column_a = 2
        column_b = 3
        column_cat = 5
        column_product = 4
        actual_order_in_a = 1
        actual_order_in_b = 0
        actual_order_in_cat = 1

        # finding the order in A
        order_in_a_experiment = Experiment('A', [exp_1, exp_2], substrate_index=column_a,
                                           product_index=column_product)
        order_in_a_results = order_in_a_experiment.calculate_best_result()
        plot_experiment_results(order_in_a_experiment)
        self.assertAlmostEqual(order_in_a_results.order, actual_order_in_a, places=1)

        # finding the order in B
        order_in_b_experiment = Experiment('B',
                                           [exp_1, exp_3],
                                           substrate_index=column_b,
                                           product_index=column_product)
        order_in_b_results = order_in_b_experiment.calculate_best_result()
        plot_experiment_results(order_in_b_experiment)
        self.assertAlmostEqual(order_in_b_results.order, actual_order_in_b, places=1)

        # finding the order in catalyst
        order_in_cat_experiment = Experiment('cat',
                                             [exp_3, exp_4],
                                             substrate_index=column_cat,
                                             product_index=column_product)
        order_in_cat_results = order_in_cat_experiment.calculate_best_result()
        plot_experiment_results(order_in_cat_experiment)
        self.assertAlmostEqual(order_in_cat_results.order, actual_order_in_cat, places=1)
